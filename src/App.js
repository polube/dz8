import { useState, useEffect } from "react";
import './App.css';
let mainId = 0;
function App() {
    const [albumId, setAlbumId] = useState(0);
    const [albums, setAlbums] = useState([]);
    const [photos, setPhotos] = useState([]);


    const onClick = (albumId) => (e) => {
        setAlbumId(albumId);
        mainId = albumId;
        get();
    }


    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/albums")
            .then((response) => response.json())
            .then((data) => {
                setAlbums(data);
            });
        


    });
    function get(){
        fetch("https://jsonplaceholder.typicode.com/photos")
            .then((response) => response.json())
            .then((data) => {
                setPhotos(data);
            });
    }



    return (
        <div className="gallery">
            <aside className="albums">
                <h4 className="albums__title">Albums</h4>
                <ul className="albums-list">
                    {albums.map(({ id, title }) => {
                        let classes = "albums-list__item";

                        if (albumId === id) {
                            classes += " albums-list__item--active";
                        }

                        return (
                            <li key={id} className={classes} onClick={onClick(id)}>{title}</li>
                        );
                    })}
                </ul>
            </aside>
            <main className="photos">
                {photos.map(({ albumId, url, thumbnailUrl }) => {

                    if (albumId === mainId) {

                        return (

                            < a href={url} > <img key={albumId} src={thumbnailUrl} alt="" /></a >

                        );
                    }

                })}
            </main>
        </div >
    );
}

export default App;
